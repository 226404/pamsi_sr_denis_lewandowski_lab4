#pragma once
#include <iostream>
#include <algorithm>

using namespace std;

struct TreeNode  {
	int value;
	TreeNode *parent;
	TreeNode *lChild;
	TreeNode *rChild;
	TreeNode() {
		lChild = NULL;
		rChild = NULL;
		parent = NULL;
	}
	~TreeNode() {}
};

class BinaryTree {
	TreeNode* root;

public:
	BinaryTree(int value) {
		root = new TreeNode;
		root->parent = NULL;
		root->value = value;
	}
	TreeNode* getRoot() { return root; }
	int getRootValue();
	void add(TreeNode *parent, int value);
	bool isExternal(TreeNode *node);
	int height(TreeNode *v);
	void remove(TreeNode *node);
};

int BinaryTree::getRootValue()
{
	return root->value;
}

void BinaryTree::add(TreeNode * parent, int value)
{
	if (parent->lChild == NULL) {
		TreeNode *newNode = new TreeNode;
		newNode->value = value;
		newNode->parent = parent;
		parent->lChild = newNode;
	}
	else if (parent->rChild == NULL) {
		TreeNode *newNode = new TreeNode;
		newNode->value = value;
		newNode->parent = parent;
		parent->rChild = newNode;
	}
	else cout << "Podany rodzic ma juz oboje dzieci!" << endl;
}

bool BinaryTree::isExternal(TreeNode* node)
{
	if ((node->lChild == NULL) && (node->rChild == NULL))
		return true;
	else 
		return false;
}

inline int BinaryTree::height(TreeNode *v)
{
	if(isExternal(v))
	return 0;
	else {
		int h = 0;
		if(v->lChild) h = max(h, height(v->lChild));
		if(v->rChild) h = max(h, height(v->rChild));
		return 1 + h;
	}
}

inline void BinaryTree::remove(TreeNode *node_to_remove)
{
	if (node_to_remove) {
		remove(node_to_remove->lChild);
		remove(node_to_remove->rChild);
		if(node_to_remove->parent->lChild == node_to_remove) node_to_remove->parent->lChild = NULL;
		if(node_to_remove->parent->rChild == node_to_remove) node_to_remove->parent->rChild = NULL;
		delete node_to_remove;
	}
}

