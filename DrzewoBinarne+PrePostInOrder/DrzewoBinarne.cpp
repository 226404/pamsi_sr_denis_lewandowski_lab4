// DrzewoBinarne.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DrzewoBinarne.h"
#include "Order.h"

using namespace std;

int main()
{
	BinaryTree drzewo(10);

	drzewo.add(drzewo.getRoot(), 1);
	drzewo.add(drzewo.getRoot(), 2);
	drzewo.add(drzewo.getRoot()->lChild, 11);
	drzewo.add(drzewo.getRoot()->lChild, 12);
	drzewo.add(drzewo.getRoot()->lChild->lChild, 111);
	drzewo.add(drzewo.getRoot()->lChild->lChild, 112);
	drzewo.add(drzewo.getRoot()->lChild->lChild->rChild, 1111);
	drzewo.add(drzewo.getRoot()->lChild->lChild->rChild, 1112);


//	drzewo.remove(drzewo.getRoot()->rChild);

	cout << "PRE ORDER: ";
	preOrder(drzewo.getRoot());
	cout << endl << endl;
	cout << "IN ORDER: ";
	inOrder(drzewo.getRoot());
	cout << endl << endl;
	cout << "POST ORDER: ";
	postOrder(drzewo.getRoot());
	cout << endl;

	drzewo.remove(drzewo.getRoot()->lChild->lChild);
	cout << endl << endl;
	cout << "Po usunieciu: " << endl;
	postOrder(drzewo.getRoot());

//	cout << "Wartosc: " << drzewo.getRoot()->lChild->lChild->rChild->value<< endl;
//	cout << "Puste? : " << drzewo.isExternal(drzewo.getRoot()->lChild->lChild->rChild);

//	cout << "Dlugosc drzewka: " << drzewo.height(drzewo.getRoot()) << endl;
//	delete drzewo.getRoot()->lChild->lChild->rChild->lChild;
//	cout << "Dlugosc drzewka: " << drzewo.height(drzewo.getRoot()) << endl;


	getchar();
    return 0;
}

