#pragma once
#include <iostream>
#include "DrzewoBinarne.h"

void preOrder(TreeNode *node) {
	if (node) {
		cout << node->value << " ";
		preOrder(node->lChild);
		preOrder(node->rChild);
	}
}

void inOrder(TreeNode *node) {
	if (node) {
		inOrder(node->lChild);
		cout << node->value << " ";
		inOrder(node->rChild);
	}
}

void postOrder(TreeNode *node) {
	if (node) {
		postOrder(node->lChild);
		postOrder(node->rChild);
		cout << node->value << " ";
	}
}