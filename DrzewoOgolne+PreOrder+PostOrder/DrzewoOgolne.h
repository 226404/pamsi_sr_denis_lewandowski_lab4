#pragma once

#include <vector>
#include "stdafx.h"
#include <algorithm>

using namespace std;

struct TreeNode
{
	int value;
	TreeNode* parent;
	vector <TreeNode*> children;

	~TreeNode() {
	if(parent)
		for (int i = 0; i < (this->children.size()); i++)
			if (this->parent->children[i] == this) this->parent->children.erase(this->parent->children.begin() + i);
	}
};


class Tree {
	TreeNode *root;

public:
	Tree(int value) { 
		root = new TreeNode;
		root->value = value;
		root->parent = NULL;
	}
	TreeNode* getRoot() { return root; }
	void add(TreeNode *parent, int value);
	int getRootValue() { return root->value; }
	bool isExternal(TreeNode *node);
	int height(TreeNode *node);
	void remove(TreeNode *node_to_remove);
};

void Tree::add(TreeNode *parent, int value)
{
	TreeNode *newNode = new TreeNode;
	newNode->value = value;
	newNode->parent = parent;
	parent->children.push_back(newNode);
}

inline bool Tree::isExternal(TreeNode * node)
{
	if (node->children.empty()) return true;
	else return false;
}

inline int Tree::height(TreeNode * node)
{
	if (isExternal(node) ) return 0;
	else {
		int h = 0;
		for (int i = 0; i < (node->children.size()); i++)
			h = max(h, height(node->children[i]));
		return 1 + h;
	}
}

inline void Tree::remove(TreeNode * node_to_remove)
{
	for (int i = 0; i < (node_to_remove->children.size()); i++)
		remove(node_to_remove->children[i]);
	for (int i = 0; i < (node_to_remove->children.size()); i++)
		if (node_to_remove->parent->children[i] == node_to_remove) node_to_remove->parent->children.erase(node_to_remove->parent->children.begin() + i);
//	delete node_to_remove;
}