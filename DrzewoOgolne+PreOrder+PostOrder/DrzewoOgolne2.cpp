// DrzewoOgolne2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "DrzewoOgolne.h"

using namespace std;

void postOrder(TreeNode *node) {
	if (node) {
		for (int i = 0; i < (node->children.size()); i++)
			postOrder(node->children[i]);
		cout << node->value << " "; // Wyswietl
	}
}

void preOrder(TreeNode *node) {
	if (node) {
		cout << node->value << " ";  // Wyswietl
		for (int i = 0; i < (node->children.size()); i++) {
			postOrder(node->children[i]);
		}
	}
}

int main()
{
	Tree drzewo(10);

	drzewo.add(drzewo.getRoot(), 1);
	drzewo.add(drzewo.getRoot(), 2);
	drzewo.add(drzewo.getRoot(), 3);
	drzewo.add(drzewo.getRoot()->children[1], 21);
	drzewo.add(drzewo.getRoot()->children[0], 11);
	drzewo.add(drzewo.getRoot()->children[0]->children[0], 111);

	cout << "Wartosc: " << drzewo.getRoot()->children.back()->value << endl;
	cout << "Wysokosc: " << drzewo.height(drzewo.getRoot()) << endl;

	postOrder(drzewo.getRoot());
	drzewo.remove(drzewo.getRoot()->children[0]);
	cout << endl << "Po usunieciu: ";
	postOrder(drzewo.getRoot());
	cout << endl << endl;
	cout << "Pre order: ";
	preOrder(drzewo.getRoot());

	cout << endl << endl;
//	inOrder(drzewo.getRoot());

	getchar();
    return 0;
}

