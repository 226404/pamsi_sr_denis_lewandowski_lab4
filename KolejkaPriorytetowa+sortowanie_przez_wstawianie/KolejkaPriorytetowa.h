#pragma once
#include "stdafx.h"
#include <iterator>
#include <iostream>

using namespace std;

class KolejkaPriorytetowa;

struct Node {
	friend class KolejkaPriorytetowa;
	int value;
	int key;
	Node *next;
	Node *prev;
};

class KolejkaPriorytetowa {
	Node *header;
	Node *trailer;

public:
	KolejkaPriorytetowa() {
		header = new Node;
		trailer = new Node;
		header->prev = NULL;
		trailer->next = NULL;
		header->next = trailer;
		trailer->prev = header;
	}

	Node *getHeader() { return header; }
	Node *getTrailer() { return trailer; }
	void addAfter(Node *p, Node *v);
	Node *min();
	void add(int key, int value);
	void remove();
};


void KolejkaPriorytetowa::addAfter(Node *p, Node *v)
{
		v->prev = p;
	    v->next = p->next;
		p->next = v;
		v->next->prev = v;
}

inline Node * KolejkaPriorytetowa::min()
{
	if(header->next != trailer)
	return header->next;
	else {
		cout << "Kolejka pusta!" << endl;
		return header->next;
	}
}

inline void KolejkaPriorytetowa::add(int key, int value)
{
	Node *newNode = new Node;
	Node *tmp = new Node;
	tmp = header->next; 
	newNode->key = key;
	newNode->value = value;
	if (tmp == trailer) {
		addAfter(getHeader(), newNode);
		return;
	}
 	while (tmp != trailer) {
		if (key <= tmp->key) {
			addAfter(tmp->prev, newNode);
			return;
		}
		tmp = tmp->next;
	}
	addAfter(trailer->prev, newNode);
	//delete tmp;
}

inline void KolejkaPriorytetowa::remove()
{
	Node *tmp = new Node;
	if (header->next != trailer) {
		tmp = header->next;
		header->next = tmp->next;
		header->next->prev = header;
		delete tmp;
	}
}
