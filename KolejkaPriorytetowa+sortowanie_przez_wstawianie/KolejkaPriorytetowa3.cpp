// KolejkaPriorytetowa3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "KolejkaPriorytetowa.h"

using namespace std;

void wyswietl(KolejkaPriorytetowa kol) {
	Node *tmp = new Node;
	tmp = kol.getHeader()->next;
	while (tmp != kol.getTrailer()) {
		cout << "Wezel klucz: " << tmp->key << "  wartosc: " << tmp->value << endl;
		tmp = tmp->next;
	}
}

//Sortowanie przez wstawianie z wykorzystaniem kolejki priorytetowej
void PiorityQueueSort(int tab[], int size) {
	KolejkaPriorytetowa kol;
	for (int i = 0; i < size; i++) {
		kol.add(tab[i], 10);
	}
	for (int i = 0; i<size; i++) {
		tab[i] = kol.getHeader()->next->key;
		kol.remove();
	}
}
int main()
{
	KolejkaPriorytetowa kolejka;
	
	kolejka.add(973, 100);
	kolejka.add(5, 99);
	kolejka.add(6, 102);
	kolejka.add(3, 10);
	kolejka.add(3, 20);
	kolejka.add(1, 10);
	kolejka.add(0, 20);
	kolejka.add(2, 20);
	kolejka.add(999, 1);
	kolejka.add(999, 1);
	kolejka.add(0, 1);

//	cout << endl << endl;
	wyswietl(kolejka);
	


	int tab[] = { 2, 3, 1, 4, 12, 6, 5, 11, 9, 7, 8, 0, 10 };
	int size = sizeof(tab) / sizeof(tab[0]);
	PiorityQueueSort(tab, size); // sortowanie przez wstawianie

	for (int i = 0; i < size; i++) {
		cout << tab[i] << "\t";
	}

	getchar();
    return 0;
}

